var system = require('system');
var page = require('webpage').create();

page.open(system.args[1], function (status) {
	if (status !== "success") {
		console.log("Unable to access network");
	} else {
		// function waitFor(testFx, onReady, timeOutMillis)
		waitFor(function() {
			return page.evaluate(function() {
// https://cloclo58.cloud.mail.ru/weblink/view//HX8f/JX6nypTog
				return (page.content.indexOf("weblink") != -1);
			});
		}, function() {
			console.log(page.content);
			// phantom.exit();
		}, 2000);
	}
});
