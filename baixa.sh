#!/bin/bash

Download() {
	local title="${1}" \
		url="${2}"
	echo "${title}"
	#set -x
	data="$(./phantomjs-2.1.1-linux-x86_64/bin/phantomjs "save_page.js" "${url}")" || \
		continue
	if [ ! -s "${title}.mp4" \
	-o -e "${title}.mp4.aria2" ]; then
		mp4="$(sed -nre '\|<source src="(https://[^"]*)".*|{s||\1|;p;q}' <<< "${data}")"
		vtt="$(sed -nre '\|<track kind="captions".*src="..([^"]*)".*|{s||https://gofilmes.me/\1|;p;q}' <<< "${data}")"
		if [ -n "${mp4}" ]; then
			echo "mp4=${title}'${mp4}'"
			rm -f "${title}.mp4"* || :
			aria2c --continue -o "${title}.mp4" "${mp4}" >&2
		else
			grep -sF '<source src=' <<< "${data}" >&2 || :
		fi
	fi
	[ -s "${title}.pt.vtt" ] || {
		echo "vtt=${title}'${vtt}'"
		rm -f "${title}.pt.vtt" || :
		aria2c --continue -o "${title}.pt.vtt" "${vtt}" >&2
	}
}

JobsRemaining() {
	local job pid jobsR="${Jobs}"
	Jobs=""
	for job in ${jobsR}; do
		pid="$(cut -f 1 -s -d '-' <<< "${job}")"
		if kill -s 0 ${pid} > /dev/null 2>&1; then
			Jobs="${Jobs}${job} "
		else
			wait ${pid} > /dev/null 2>&1 || :
		fi
	done
	test -n "${Jobs}"
}

Main() {
	local d="y" \
		Jobs=""
	while [ -n "$d" ]; do
		d=""
		while IFS=$'\t' read -r epi name url; do
			[ -n "${epi}" ] || \
				continue
			epi="$(tr -s '[:blank:]-' '_' <<< "${epi}")"
			title="${epi}-${name}"
			if [ ! -s "${title}.mp4" \
			-o ! -s "${title}.pt.vtt" \
			-o -e "${title}.mp4.aria2" ]; then
				grep -qswF "-${epi}" <<< "${Jobs}" || {
					d="y"
					Download "${title}" "${url}" &
					Jobs="${Jobs}${!}-${epi} "
				}
			fi
			while JobsRemaining && \
			[ $(wc -w <<< "${Jobs}") -ge ${MAXJOBS} ]; do
				wait $(printf '%s\n' ${Jobs} | cut -f 1 -s -d '-') || :
			done
		done < chicago-fire-temps-epis.html
	done

	while JobsRemaining; do
		wait $(printf '%s\n' ${Jobs} | cut -f 1 -s -d '-') || :
	done
}

readonly MAXJOBS=3

Main
