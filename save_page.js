var system = require('system');
var page = require('webpage').create();

page.open(system.args[1], function (status) {
	// Check for page load success
	if (status !== "success") {
		console.log("Unable to access network");
	} else {
// https://cloclo58.cloud.mail.ru/weblink/view//HX8f/JX6nypTog
		setTimeout(function() {
			console.log(page.content);
			phantom.exit();
		}, 2000);
	}
});
